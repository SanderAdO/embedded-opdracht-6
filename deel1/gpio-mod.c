#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/stat.h>
#include <linux/gpio.h>

//Struct array for outputs defining BCM pin numbers, direction + initial state and label
static struct gpio aLeds[] = {
    {17, GPIOF_OUT_INIT_HIGH, "BCM 17"},
    {27, GPIOF_OUT_INIT_HIGH, "BCM 27"},
};

int outputs[2] = {-1, -1};
static int arr_argc_outputs = 0;
int states[2] = {-1, -1};
static int arr_argc_states = 0;

module_param_array(outputs, int, &arr_argc_outputs, 0000);
MODULE_PARM_DESC(outputs, "outputs");
module_param_array(states, int, &arr_argc_states, 0000);
MODULE_PARM_DESC(states, "states");

static int __init gpiomod_init(void)
{
    int ret = 0;
    int i;

    printk(KERN_INFO "%s\n---\n", __func__);

    for (i = 0; i < (sizeof outputs / sizeof(int)); i++)
    {
        aLeds[i].gpio = outputs[i];
        printk(KERN_INFO "outputs[%d] = %d\n", i, outputs[i]);
        gpio_direction_output(aLeds[i].gpio, 0);
    }
    printk(KERN_INFO "got %d arguments for outputs.\n", arr_argc_outputs);

    i = 0;

    for (i = 0; i < (sizeof states / sizeof(int)); i++)
    {
        printk(KERN_INFO "states[%d] = %d\n", i, states[i]);
    }
    printk(KERN_INFO "got %d arguments for states.\n", arr_argc_states);

    if (ret)
    {
        printk(KERN_ERR "Unable to request GPIOs: %d\n", ret);
    }

    for (i = 0; i < ARRAY_SIZE(aLeds); i++)
    {
        gpio_set_value(aLeds[i].gpio, states[i]);
    }
    return ret;
}

static void __exit gpiomod_exit(void)
{
    int i;

    printk(KERN_INFO "%s\n---\n", __func__);

    // turn all GPIO's off on program shutdown
    for (i = 0; i < ARRAY_SIZE(aLeds); i++)
    {
        gpio_set_value(aLeds[i].gpio, 0);
    }

    // put GPIO's back to input
    for (i = 0; i < ARRAY_SIZE(aLeds); i++)
    {
        gpio_direction_input(aLeds[i].gpio);
    }

    // free all gpio's (unregister) from the array
    gpio_free_array(aLeds, ARRAY_SIZE(aLeds));
}

MODULE_AUTHOR("Sander AdO");
MODULE_DESCRIPTION("Embedded Systems - Opdracht 6");
MODULE_LICENSE("GPL");

module_init(gpiomod_init);
module_exit(gpiomod_exit);